# Flutter-Course-Projects

## Project 1: I Am Rich - How to Create Flutter Apps from Scratch

* [Project Link](https://gitlab.com/aqib_hassan/i_am_rich.git)


## Project 2: MiCard - How to Build Beautiful UIs with Flutter Widgets

* [Project Link](https://gitlab.com/aqib_hassan/mi_card_flutter.git)


## Project 3:  Dicee - Building Apps with State

* [Project Link](https://gitlab.com/aqib_hassan/dicee-flutter.git)

## Project 4: Xylophone - Using Flutter and Dart Packages to Speed Up Development

* [Project Link](https://gitlab.com/aqib_hassan/xylophone_flutter.git)

## Project 5: Quizzler - Modularising & Organising Flutter Code

* [Project Link](https://gitlab.com/aqib_hassan/quizzler_flutter.git)

## Project 6: BMI Calculator - Building Flutter UIs for Intermediates

* [Project Link](https://gitlab.com/aqib_hassan/bmi_calculator_flutter.git)

## Project 7: Clima - Powering Your Flutter App with Live Weather Web Data

* [Project Link](https://gitlab.com/aqib_hassan/climate_flutter.git)

## Project 8: Flash Chat - Flutter x Firebase Cloud Firestore

* [Project Link](https://gitlab.com/aqib_hassan/flash_chat_flutter.git)

## Project 9: Flutter State Management

* [Project Link](https://gitlab.com/aqib_hassan/state_management_flutter.git)

